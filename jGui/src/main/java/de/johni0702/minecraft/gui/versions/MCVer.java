package de.johni0702.minecraft.gui.versions;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.crash.CrashReportCategory;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.ReadableColor;

import net.minecraft.client.renderer.vertex.DefaultVertexFormats;

import java.util.concurrent.Callable;

/**
 * Abstraction over things that have changed between different MC versions.
 */
public class MCVer {
    public static ScaledResolution newScaledResolution(Minecraft mc) {
        if (mc == null ) {
            return new ScaledResolution(Minecraft.getMinecraft());
        }
        return new ScaledResolution(mc);
    }

    public static void addDetail(CrashReportCategory category, String name, Callable<String> callable) {
        category.addCrashSectionCallable(name, callable);
    }

    public static void drawRect(int right, int bottom, int left, int top) {
        Tessellator tessellator = Tessellator.getInstance();
        WorldRenderer vertexBuffer = tessellator.getWorldRenderer();
        vertexBuffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION);
        vertexBuffer.pos(right, top, 0).endVertex();
        vertexBuffer.pos(left, top, 0).endVertex();
        vertexBuffer.pos(left, bottom, 0).endVertex();
        vertexBuffer.pos(right, bottom, 0).endVertex();
        tessellator.draw();
    }

    public static void drawRect(int x, int y, int width, int height, ReadableColor tl, ReadableColor tr, ReadableColor bl, ReadableColor br) {
        Tessellator tessellator = Tessellator.getInstance();
        WorldRenderer vertexBuffer = tessellator.getWorldRenderer();
        vertexBuffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
        vertexBuffer.pos(x, y + height, 0).color(bl.getRed(), bl.getGreen(), bl.getBlue(), bl.getAlpha()).endVertex();
        vertexBuffer.pos(x + width, y + height, 0).color(br.getRed(), br.getGreen(), br.getBlue(), br.getAlpha()).endVertex();
        vertexBuffer.pos(x + width, y, 0).color(tr.getRed(), tr.getGreen(), tr.getBlue(), tr.getAlpha()).endVertex();
        vertexBuffer.pos(x, y, 0).color(tl.getRed(), tl.getGreen(), tl.getBlue(), tl.getAlpha()).endVertex();
        tessellator.draw();
    }

    public static FontRenderer getFontRenderer() {
        return Minecraft.getMinecraft().fontRendererObj;
    }
}
