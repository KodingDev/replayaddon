package com.replaymod.render;

import cc.hyperium.event.InitializationEvent;
import cc.hyperium.event.PreInitializationEvent;
import com.google.common.eventbus.Subscribe;
import com.replaymod.config.Configuration;
import com.replaymod.core.ReplayMod;
import com.replaymod.render.utils.RenderJob;
import com.replaymod.replay.events.ReplayCloseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.crash.CrashReport;
import net.minecraft.util.ReportedException;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ReplayModRender {
    public static final String MOD_ID = "replaymod-render";

    public static ReplayModRender instance;

    private ReplayMod core;

    public static Logger LOGGER = LogManager.getLogger("Replay Mod");

    private Configuration configuration;
    private final List<RenderJob> renderQueue = new ArrayList<>();

    public ReplayMod getCore() {
        return core;
    }

    @Subscribe
    public void preInit(PreInitializationEvent event) {
        ReplayModRender.instance = this;
        core = ReplayMod.instance;
        configuration = new Configuration(new File(Minecraft.getMinecraft().mcDataDir, "hyperium/replaymod-render.json"));

    }
    @Subscribe
    public void onInit(InitializationEvent e) {
        core.getSettingsRegistry().register(Setting.class);
    }

    @Subscribe
    public void onReplayClose(ReplayCloseEvent.Post event) {
        renderQueue.clear();
    }

    public File getVideoFolder() {
        String path = core.getSettingsRegistry().get(Setting.RENDER_PATH);
        File folder = new File(path.startsWith("./") ? core.getMinecraft().mcDataDir : null, path);
        try {
            FileUtils.forceMkdir(folder);
        } catch (IOException e) {
            throw new ReportedException(CrashReport.makeCrashReport(e, "Cannot create video folder."));
        }
        return folder;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public List<RenderJob> getRenderQueue() {
        return renderQueue;
    }
}
