package com.replaymod.replay.events;

import cc.hyperium.event.CancellableEvent;

public abstract class ReplayDispatchKeypressesEvent extends CancellableEvent {

    public static class Pre extends ReplayDispatchKeypressesEvent {}
}
