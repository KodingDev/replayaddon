package com.replaymod.replay.events;

import cc.hyperium.event.CancellableEvent;
import com.replaymod.replay.camera.CameraEntity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ReplayChatMessageEvent extends CancellableEvent {
    @Getter
    private final CameraEntity cameraEntity;
}
