package com.replaymod.hyperiumcompat;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MethodWrapper2 {
    private Method inner;
    private Object owner;
    MethodWrapper2(Method inner, Object object) {
        this.inner = inner;
        this.owner = object;
    }
    public Object invoke(Object... params) {
        try {
            return this.inner.invoke(this.owner, params);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }
}
