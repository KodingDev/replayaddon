package com.replaymod.hyperiumcompat;

import java.lang.reflect.Field;

public class FieldWrapper2 {
    private Field inner;
    private Object owner;
    FieldWrapper2(Field inner, Object owner) {
        this.owner = owner;
        this.inner = inner;
    }
    public Object get() {
        try {
            return inner.get(owner);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }
    public FieldWrapper2 set(Object value) {
        try {
            this.inner.set(owner, value);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return this;
    }
}
