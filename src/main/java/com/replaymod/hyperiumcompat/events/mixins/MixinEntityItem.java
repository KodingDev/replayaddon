package com.replaymod.hyperiumcompat.events.mixins;

import com.replaymod.hyperiumcompat.events.ItemPickupEvent;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(EntityItem.class)
public class MixinEntityItem {

    /**
     * Called by a player entity when they collide with an entity
     */
    @Inject(method = "onCollideWithPlayer", at = @At("HEAD"))
    public void onItemPickup(EntityPlayer entityIn, CallbackInfo callbackInfo) {
        cc.hyperium.event.EventBus.INSTANCE.post(new ItemPickupEvent((EntityItem) (Object) this, entityIn));
    }
}
