package com.replaymod.hyperiumcompat.events;

import lombok.RequiredArgsConstructor;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;

@RequiredArgsConstructor
public class ItemPickupEvent {
    public final EntityItem pickedUp;
    public final EntityPlayer player;
}
