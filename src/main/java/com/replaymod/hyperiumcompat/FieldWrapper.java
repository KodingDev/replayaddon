package com.replaymod.hyperiumcompat;

import java.lang.reflect.Field;

public class FieldWrapper{
    private Field inner;
    FieldWrapper(Field inner) {
        this.inner = inner;
    }
    public Object get(Object obj) {
        try {
            return inner.get(obj);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }
    public FieldWrapper set(Object obj, Object value) {
        try {
            this.inner.set(obj, value);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return this;
    }
}
