package com.replaymod.core;

import cc.hyperium.Hyperium;
import cc.hyperium.event.RenderEvent;
import cc.hyperium.handlers.handlers.keybinds.HyperiumBind;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.eventbus.Subscribe;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import net.minecraft.crash.CrashReport;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.util.ReportedException;
import org.lwjgl.input.Keyboard;

public class KeyBindingRegistry {
    private Map<String, HyperiumBind> keyBindings = new HashMap<String, HyperiumBind>();
    private Multimap<HyperiumBind, Runnable> keyBindingHandlers = ArrayListMultimap.create();
    private Multimap<HyperiumBind, Runnable> repeatedKeyBindingHandlers = ArrayListMultimap.create();
    private Multimap<Integer, Runnable> rawHandlers = ArrayListMultimap.create();

    public void registerKeyBinding(String name, int keyCode, Runnable whenPressed) {
        keyBindingHandlers.put(registerKeyBinding(name, keyCode), whenPressed);
    }

    public void registerRepeatedKeyBinding(String name, int keyCode, Runnable whenPressed) {
        repeatedKeyBindingHandlers.put(registerKeyBinding(name, keyCode), whenPressed);
    }

    private HyperiumBind registerKeyBinding(String name, int keyCode) {
        HyperiumBind keyBinding = keyBindings.get(name);
        if (keyBinding == null) {
            keyBinding = new HyperiumBind(name, keyCode, "replaymod.title");
            keyBindings.put(name, keyBinding);
            Hyperium.INSTANCE.getHandlers().getKeybindHandler().registerKeyBinding(keyBinding);
        }
        return keyBinding;
    }

    public void registerRaw(int keyCode, Runnable whenPressed) {
        rawHandlers.put(keyCode, whenPressed);
    }

    public Map<String, HyperiumBind> getKeyBindings() {
        return Collections.unmodifiableMap(keyBindings);
    }

    @Subscribe
    public void onKeyInput(cc.hyperium.event.KeypressEvent e) {
        handleKeyBindings();
        handleRaw();
    }

    @Subscribe
    public void onTick(RenderEvent event) {
        handleRepeatedKeyBindings();
    }

    public void handleRepeatedKeyBindings() {
        for (Map.Entry<HyperiumBind, Collection<Runnable>> entry : repeatedKeyBindingHandlers.asMap().entrySet()) {
            if (Keyboard.isKeyDown(entry.getKey().getKeyCode())) {
                invokeKeyBindingHandlers(entry.getKey(), entry.getValue());
            }
        }
    }

    public void handleKeyBindings() {
        for (Map.Entry<HyperiumBind, Collection<Runnable>> entry : keyBindingHandlers.asMap().entrySet()) {
            while (entry.getKey().wasPressed()) {
                invokeKeyBindingHandlers(entry.getKey(), entry.getValue());
            }
        }
    }

    private void invokeKeyBindingHandlers(HyperiumBind keyBinding, Collection<Runnable> handlers) {
        for (final Runnable runnable : handlers) {
            try {
                runnable.run();
            } catch (Throwable cause) {
                CrashReport crashReport = CrashReport.makeCrashReport(cause, "Handling Key Binding");
                CrashReportCategory category = crashReport.makeCategory("Key Binding");
                category.addCrashSection("Key Binding", keyBinding);
                category.addCrashSectionCallable("Handler", runnable::toString);
                throw new ReportedException(crashReport);
            }
        }
    }

    public void handleRaw() {
        int keyCode = Keyboard.getEventKey() == 0 ? Keyboard.getEventCharacter() + 256 : Keyboard.getEventKey();
        for (final Runnable runnable : rawHandlers.get(keyCode)) {
            try {
                runnable.run();
            } catch (Throwable cause) {
                CrashReport crashReport = CrashReport.makeCrashReport(cause, "Handling Raw Key Binding");
                CrashReportCategory category = crashReport.makeCategory("Key Binding");
                category.addCrashSection("Key Code", keyCode);
                category.addCrashSectionCallable("Handler", new Callable() {
                    @Override
                    public Object call() throws Exception {
                        return runnable;
                    }
                });
                throw new ReportedException(crashReport);
            }
        }
    }
}