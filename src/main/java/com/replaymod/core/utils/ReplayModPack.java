package com.replaymod.core.utils;

import com.google.common.collect.ImmutableSet;
import java.io.File;
import java.util.Map;
import java.util.Set;
import net.minecraft.client.resources.DefaultResourcePack;

public class ReplayModPack extends DefaultResourcePack {

  private static final Set<String> DOMAINS = ImmutableSet.of("replaymod", "jgui");

  public ReplayModPack(Map<String, File> p_i46346_1_) {
    super(p_i46346_1_);
  }
  @Override
  public Set<String> getResourceDomains() {
    return DOMAINS;
  }
}
