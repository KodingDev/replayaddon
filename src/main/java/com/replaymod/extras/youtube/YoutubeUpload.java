package com.replaymod.extras.youtube;

import com.replaymod.core.ReplayMod;
import com.replaymod.core.utils.ReplayEventBus;
import com.replaymod.extras.Extra;
import de.johni0702.minecraft.gui.element.GuiButton;

public class YoutubeUpload implements Extra {
    @Override
    public void register(ReplayMod mod) throws Exception {
        ReplayEventBus.INSTANCE.register(this);
    }

    /*
    @Subscribe
    public void onGuiOpen(GuiScreenEvent.InitGuiEvent.Post event) {
        if (GuiScreen.from(event.gui) instanceof GuiRenderingDone) {
            GuiRenderingDone gui = (GuiRenderingDone) GuiScreen.from(event.gui);
            // Check if there already is a youtube button
            if (gui.actionsPanel.getChildren().stream().anyMatch(it -> it instanceof YoutubeButton)) {
                return; // Button already added
            }
            // Add the Upload to YouTube button to actions panel
            gui.actionsPanel.addElements(null,
                    new YoutubeButton().onClick(() ->
                            new GuiYoutubeUpload(gui, gui.videoFile, gui.videoFrames, gui.settings).display()
                    ).setSize(200, 20).setI18nLabel("replaymod.gui.youtubeupload"));
        }
    }
     */

    private static class YoutubeButton extends GuiButton {}
}
